package socialNetwork;
/**
 * Create User Application for whatsApp
 * @author herukala1
 *
 */

public class User {
	public String name;     	//To represents userName.
    public String phone;		//To represents phoneNb.
	public String password;		//To represents password.
	/**
	 * By using instance variables we printing the variables
	 * @param name
	 * @param phone
	 * @param password
	 * @throws Exception
	 */
   public   User(String name, String phone, String password) throws Exception {
		if(name.trim().length() < 2 || name.trim().length() > 40) {
			// validation of name failure
			throw new Exception("Name is too short.");
		}
		else if(phone.trim().length() < 2 || phone.trim().length() > 260) {
			// validation of phone failure
			throw new Exception("phone is too short");
		}
		else if(password.length()<1 || password.length()>9) {
			// validation of price failure
			throw new Exception("password is too low or negative");
		} else {
	}
		this.name=name;
		this.phone=phone;
		this.password=password;
	}

	@Override
	public String toString() {
		
		String userreturn = "";
		userreturn+="name     :   "+this.name+"\n";
		userreturn+="phone    :   "+this.phone+"\n";
		userreturn+="password :   "+this.password+"\n";
		
		return userreturn;
	}

	
}
	




package socialNetwork;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
/**
 * Here we are using ArrayList to print all the users.
 * @author herukala1
 * 
 */
public class Message {
	ArrayList<String> messageList = new ArrayList<String>();
	private String sendmsg1;
	private String recvmsg1;
	private String msgbody1;
	
public Message(String sendmsg, String recvmsg, String msgbody) {
		// TODO Auto-generated constructor stub
	this.sendmsg1=sendmsg;
	this.recvmsg1=recvmsg;
	this.msgbody1=msgbody;
	System.out.println(this);
	}
public String toString() {
	String returnToMe=" ";
	returnToMe+="sendmsg:      "+this.sendmsg1+"\n";
	returnToMe+="recvmsg:      "+this.recvmsg1+"\n";
	returnToMe+="msgbody:      "+this.msgbody1+"\n";
	return returnToMe;
}

/**
 * By using ArrayList we can add multiple users.
 * @param sendmsg
 * @param recvmsg
 * @param msgbody
 * @return
 */

/**
 * By using iterator To print all the messages
 */
public  void printAllmessages() {
	  for (Iterator iter = messageList.iterator(); 
			  iter.hasNext();) {
		User currentmsg = (User)iter.next();
		System.out.println(currentmsg);	
	}
}
	
/**public void returnMessage(){
	try {
		Message msg1=new Message("8688027633", "7013462028", "hi");
		System.out.println(msg1);
		Message msg2=new Message("8179262028", "7013462028", "hello");
		System.out.println(msg2);
		Message msg3=new Message("8688027633", "7013462028", "coffe");
		System.out.println(msg3);
		
	} catch (Exception e) {
		System.out.println(e.getLocalizedMessage());

 }*/


 }
 

	
	